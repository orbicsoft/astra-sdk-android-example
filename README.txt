Astra APK Test app
Copyright (c) 2015 Orbbec

Building
-----------
Make sure the environment variable ASTRA_ANDROID_SDK is set to the Astra Android SDK path.
For example, on OS X:

export ASTRA_ANDROID_SDK=/Users/joshblake/projects/astra/build-android/armeabi-v7a/sdk

(This is the path that Astra's "make install" copies files to.)

Then in the top directory in this project, run the command for your platform:

Windows:
gradlew.bat build

OS X:
./gradlew build

Installing
----------
The output APK will be located in:
build/outputs/apk

From that directory, install it to your Android machine using:
adb install -r orbbec-sample-debug.apk

Notes
----------
The app requires external storage permission to write profiling results to /sdcard.
After closing or backing out of the app, it should write several profile_*.txt files.

The Astra log file will be located in:
/data/data/com.orbbec.sample/files/astra.log
