package com.orbbec.sample;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;


import com.orbbec.astra.AstraContext;

/**
 * Created by jack.xu on 2015/7/9.
 */
public class BootService extends Service {

    private final static String TAG="BootService";
    LinearLayout mFloatLayout;
    WindowManager.LayoutParams wmParams;
    WindowManager mWindowManager;


    private com.orbbec.sample.GLView glView;
    private AstraContext astraContext;
    private static final int id_depth_toggle_button = 0;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void CreateFloatView()
    {
        wmParams=new WindowManager.LayoutParams();
        mWindowManager=(WindowManager)getSystemService(Context.WINDOW_SERVICE);
        wmParams.type= WindowManager.LayoutParams.TYPE_PHONE;
        wmParams.format= PixelFormat.RGBA_8888;
        wmParams.flags= WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        wmParams.gravity=Gravity.LEFT|Gravity.TOP;
        wmParams.x=0;
        wmParams.y=0;
        wmParams.width= 300;
        wmParams.height=300;

        mFloatLayout = new LinearLayout(getApplication());
        Button b = new Button(this);
        b.setText("Toggle depth");
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("TAG","Toggle depth button clicked");
                com.orbbec.sample.NativeJNILib.toggleDepth();

            }
        });

        mFloatLayout.addView(b);
        mFloatLayout.setGravity(Gravity.LEFT | Gravity.BOTTOM);

        mWindowManager.addView(glView,wmParams);
        //mWindowManager.addView(mFloatLayout,new WindowManager().LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT));

    }

    @Override
    public void onCreate() {
        Log.v(TAG, "onCreate()");

        astraContext = new AstraContext(getApplication());
        astraContext.initialize();
        astraContext.openAllDevices();

        Log.d(TAG, "onCreate creating glview");
        glView = new com.orbbec.sample.GLView(getApplication());

        Log.d(TAG, "onCreate setting float view");
        CreateFloatView();
        super.onCreate();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.v(TAG,"onUnbind");

        return super.onUnbind(intent);
    }

    @Override
    public void onTrimMemory(int level) {
        Log.v(TAG,"onTrimMemeory level="+level);

        super.onTrimMemory(level);
    }

    @Override
    public void onLowMemory() {
        Log.v(TAG,"onLowMemory");
        super.onLowMemory();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.v(TAG,"onConfigurationChanged");

        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onDestroy() {
        Log.v(TAG,"onDestory");

        com.orbbec.sample.NativeJNILib.cleanup();
        mWindowManager.removeView(glView);
        mWindowManager.removeView(mFloatLayout);
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v(TAG,"onStartCommand");


        return super.onStartCommand(intent, flags, startId);
    }
}
