package com.orbbec.sample;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Gravity;

import com.orbbec.sample.BootService;
import com.orbbec.sample.NativeJNILib;
import com.orbbec.sample.GLView;
import com.orbbec.astra.AstraContext;

public class BootstrapperActivity extends Activity implements OnClickListener {

    private static final String TAG = "BootstrapperActivity";

    private GLView glView;
    private AstraContext astraContext;

    private static final int DEPTH_TOGGLE_BUTTON_ID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        astraContext = new AstraContext(getApplication());
        astraContext.initialize();
        astraContext.openAllDevices();

        glView = new GLView(getApplication());

        LinearLayout ll = new LinearLayout(this);
        Button b = new Button(this);
        b.setText("Toggle Depth");
        b.setOnClickListener(this);
        b.setId(DEPTH_TOGGLE_BUTTON_ID);

        ll.addView(b);
        ll.setGravity(Gravity.LEFT | Gravity.BOTTOM);
        this.addContentView(ll, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

        case DEPTH_TOGGLE_BUTTON_ID:
            Intent intent = new Intent();
            intent.setAction("com.orbbec.bootservice");
            startService(intent);
            break;

      }
    }

    @Override
    protected void onDestroy() {
        NativeJNILib.cleanup();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        glView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        glView.onResume();
    }
}
