package com.orbbec.sample;

public class NativeJNILib {
    static {
       System.loadLibrary("SensorViewer");
    }

    public static native void init();
    public static native void resize(int width, int height);
    public static native void step();
    public static native void cleanup();
    public static native void toggleDepth();
}
