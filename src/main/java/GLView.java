package com.orbbec.sample;

import android.content.Context;
import android.util.Log;
import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

public class GLView extends GLSurfaceView {
    private static final String TAG = "GLView";

    public GLView(Context context) {
        super(context);

        setEGLConfigChooser(8, 8, 8, 0, 16, 0);
        setEGLContextClientVersion(2);
        setRenderer(new GLRenderer());
    }

    private static class GLRenderer implements GLSurfaceView.Renderer {
        private static final String TAG = "GLView.GLRenderer";
        public void onDrawFrame(GL10 gl) {
            NativeJNILib.step();
        }

        public void onSurfaceChanged(GL10 gl, int width, int height) {
            Log.d(TAG, "onSurfaceChanged");
            NativeJNILib.resize(width, height);
        }

        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            Log.d(TAG, "onSurfaceCreated");
            NativeJNILib.init();
        }
    }
}
