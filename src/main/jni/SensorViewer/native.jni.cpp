#include "native.jni.h"
#include <stdlib.h>
#include "common.h"
#include <stdint.h>
#include <stdio.h>
#include "Renderer.h"
#include "AstraTest.h"
#include <Shiny.h>
#include <pthread.h>
#include "SensorData.h"

#define CONNECTION_STRING_MAX_LEN 256

static Renderer* g_renderer = nullptr;
static bool m_deviceReady = false;

static pthread_mutex_t g_dataMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t g_astraThread;
static pthread_attr_t g_threadAttribute;
volatile bool g_threadShouldRun = false;
bool g_threadCreated = false;

SensorData g_firstData;
SensorData g_secondData;
SensorData* g_frontData = &g_firstData;
SensorData* g_backData = &g_secondData;

Renderer* createRenderer()
{
    LOGI("create renderer");
    Renderer* renderer = new Renderer;
    if (!renderer->init()) {
        delete renderer;
        return nullptr;
    }
    return renderer;
}

void swap_data_buffers()
{
    PROFILE_FUNC();
    //called from the astra thread
    pthread_mutex_lock(&g_dataMutex);
    if (g_frontData == &g_firstData)
    {
        g_frontData = &g_secondData;
        g_backData = &g_firstData;
    }
    else
    {
        g_frontData = &g_firstData;
        g_backData = &g_secondData;
    }
    pthread_mutex_unlock(&g_dataMutex);
}

void* AstraThreadWorker(void* _pvArg)
{
    PROFILE_FUNC();
    LOGI("astra worker thread start");

    AstraTest astraTest;
    //ensure buffer pointers are not null
    swap_data_buffers();

    while (g_threadShouldRun)
    {
        PROFILE_BLOCK(Worker_loop);
        astraTest.update(*g_backData);

        if (g_backData->isValid)
        {
            swap_data_buffers();
            PROFILE_UPDATE();
        }
    }

    LOGI("astra worker thread stop");

    PROFILE_UPDATE();
    PROFILE_OUTPUT("/sdcard/profile_astra_apk.txt");
    return nullptr;
}

void createAstraThread()
{
    LOGI("create astra worker thread");
    g_threadShouldRun = true;
    g_threadCreated = true;
    pthread_attr_init(&g_threadAttribute);
    pthread_attr_setdetachstate(&g_threadAttribute, PTHREAD_CREATE_JOINABLE);
    pthread_create(&g_astraThread, &g_threadAttribute, &AstraThreadWorker, NULL);
}

void joinAstraThread()
{
    LOGI("join astra worker thread");
    g_threadShouldRun = false;
    pthread_join(g_astraThread, NULL);
    pthread_attr_destroy(&g_threadAttribute);
    g_threadCreated = false;
}

void destroy_resources()
{
    LOGI("destroy resources");

    joinAstraThread();

    if (g_renderer) {
        delete g_renderer;
        g_renderer = nullptr;
    }
}

void update_renderer()
{
    //called from the UI thread
    if (!g_renderer)
    {
        return;
    }

    pthread_mutex_lock(&g_dataMutex);

    SensorData* data = g_frontData;
    if (data != nullptr && data->isValid)
    {
        g_renderer->render(*data);
    }

    pthread_mutex_unlock(&g_dataMutex);
}

static void printGlString(const char* name, GLenum s)
{
    const char* v = (const char*)glGetString(s);
    LOGI("GL %s: %s\n", name, v);
}

JNIEXPORT void JNICALL Java_com_orbbec_sample_NativeJNILib_init
  (JNIEnv *, jclass)
{
    LOGI("NativeJNILib_init");

    if (g_renderer) {
        delete g_renderer;
        g_renderer = nullptr;
    }

    printGlString("Version", GL_VERSION);
    printGlString("Vendor", GL_VENDOR);
    printGlString("Renderer", GL_RENDERER);
    printGlString("Extensions", GL_EXTENSIONS);

    g_renderer = createRenderer();

    if (!g_threadCreated)
    {
        createAstraThread();
    }
}

JNIEXPORT void JNICALL Java_com_orbbec_sample_NativeJNILib_resize
  (JNIEnv *, jclass, jint width, jint height)
{
    LOGI("NativeJNILib_resize %d, %d", width, height);

    if (g_renderer)
    {
        g_renderer->resize(width, height);
    }
}

JNIEXPORT void JNICALL Java_com_orbbec_sample_NativeJNILib_step
  (JNIEnv *, jclass)
{
    update_renderer();
}

JNIEXPORT void JNICALL Java_com_orbbec_sample_NativeJNILib_cleanup
  (JNIEnv *, jclass)
{
    LOGI("NativeJNILib_cleanup");
    destroy_resources();
}

JNIEXPORT void JNICALL Java_com_orbbec_sample_NativeJNILib_toggleDepth
(JNIEnv *, jclass)
{
    LOGI("ToggleDepthVisualization");
    if (g_renderer)
    {
        g_renderer->toggle_depth_visualization();
    }
}
