#ifndef COMMON_H
#define COMMON_H

#include <android/log.h>

#ifndef LOG_TAG
#define LOG_TAG "NDK-NATIVE"
#endif

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__))

#endif
