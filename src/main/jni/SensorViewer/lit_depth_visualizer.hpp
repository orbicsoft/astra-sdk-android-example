#ifndef LITDEPTHVISUALIZER_HPP
#define LITDEPTHVISUALIZER_HPP

#include <astra/astra.hpp>
#include <cstring>
#include <algorithm>
#include <Shiny.h>

namespace samples { namespace common {

    using namespace astra;

    class LitDepthVisualizer
    {
    public:
        LitDepthVisualizer()
        {
            PROFILE_FUNC();
            lightColor_ = {210, 210, 210};
            lightVector_ = Vector3f{0.44022f, -0.17609f, 0.88045f};
            ambientColor_ = {30, 30, 30};
        }

        void set_light_color(const astra_rgb_pixel_t& color)
        {
            lightColor_ = color;
        }

        void set_light_direction(const Vector3f& direction)
        {
            lightVector_ = direction;
        }

        void set_ambient_color(const astra_rgb_pixel_t& color)
        {
            ambientColor_ = color;
        }

        void set_blur_radius(unsigned int radius)
        {
            blurRadius_ = radius;
        }

        void updateLitPoints(const PointFrame& pointFrame, SensorData& data);
        void updateDepth(const DepthFrame& depthFrame, SensorData& data);
        void update_debug_handframe(const DebugHandFrame& debugHandFrame, SensorData& data);

    private:
        using VectorMapPtr = std::unique_ptr<Vector3f[]>;
        VectorMapPtr normalMap_{nullptr};
        VectorMapPtr blurNormalMap_{nullptr};
        size_t normalMapLength_{0};

        Vector3f lightVector_;
        unsigned int blurRadius_{1};
        astra_rgb_pixel_t lightColor_;
        astra_rgb_pixel_t ambientColor_;

        void prepare_buffer(size_t width, size_t height, SensorData& data);
        void calculate_normals(const PointFrame& pointFrame);
    };

    void box_blur(const Vector3f* in,
                  Vector3f* out,
                  const size_t width,
                  const size_t height,
                  const int blurRadius = 1)
    {
        PROFILE_FUNC();

        const size_t maxY = height - blurRadius;
        const size_t maxX = width - blurRadius;

        out += blurRadius * width;

        for (size_t y = blurRadius; y < maxY; ++y)
        {
            out += blurRadius;
            for (size_t x = blurRadius; x < maxX; ++x, ++out)
            {
                Vector3f normAvg;

                size_t index = x - blurRadius + (y - blurRadius) * width;
                const Vector3f* in_row = in + index;

                for (int dy = -blurRadius; dy <= blurRadius; ++dy, in_row += width)
                {
                    const Vector3f* in_kernel = in_row;
                    for (int dx = -blurRadius; dx <= blurRadius; ++dx, ++in_kernel)
                    {
                        normAvg += *in_kernel;
                    }
                }

                *out = Vector3f::normalize(normAvg);
            }
            out += blurRadius;
        }
    }

    void box_blur_fast(const Vector3f* in,
                       Vector3f* out,
                       const size_t width,
                       const size_t height)
    {
        PROFILE_FUNC();

        const size_t maxY = height - 1;
        const size_t maxX = width - 1;

        const Vector3f* in_row = in + width;
        Vector3f* out_row = out;

        memset(out, 0, width * height * sizeof(Vector3f));

        for (size_t y = 1; y < height; ++y, in_row += width, out_row += width)
        {
            const Vector3f* in_left = in_row;
            const Vector3f* in_mid = in_row + 1;

            Vector3f* out_up = out_row - width;
            Vector3f* out_mid = out_row;

            Vector3f xKernelTotal;

            for (size_t x = 1; x < width; ++x)
            {
                xKernelTotal += *in_mid;

                *out_up += xKernelTotal;
                *out_mid += xKernelTotal;

                xKernelTotal -= *in_left;

                ++in_left;
                ++in_mid;
                ++out_up;
                ++out_mid;
            }
        }
    }

    void LitDepthVisualizer::calculate_normals(const PointFrame& pointFrame)
    {
        PROFILE_FUNC();
        const Vector3f* positionMap = pointFrame.data();

        const int width = pointFrame.width();
        const int height = pointFrame.height();

        const int numPixels = width * height;

        if (normalMap_ == nullptr || normalMapLength_ != numPixels)
        {
            PROFILE_BLOCK(init_norm_mem);
            normalMap_ = std::make_unique<Vector3f[]>(numPixels);
            blurNormalMap_ = std::make_unique<Vector3f[]>(numPixels);

            std::fill(blurNormalMap_.get(), blurNormalMap_.get() + numPixels, Vector3f::zero());

            normalMapLength_ = numPixels;
        }

        Vector3f* normMap = normalMap_.get();

        //top row
        for (int x = 0; x < width; ++x)
        {
            *normMap = Vector3f::zero();
            ++normMap;
        }

        for (int y = 1; y < height - 1; ++y)
        {
            //first pixel at start of row
            *normMap = Vector3f::zero();
            ++normMap;

            //Initialize pointer arithmetic for the x=0 position
            const Vector3f* p_point = positionMap + y * width;
//            const Vector3f* p_pointLeft = p_point - 1;
            const Vector3f* p_pointRight = p_point + 1;
//            const Vector3f* p_pointUp = p_point - width;
            const Vector3f* p_pointDown = p_point + width;

            for (int x = 1; x < width - 1; ++x)
            {
//                ++p_pointLeft;
                ++p_point;
                ++p_pointRight;
//                ++p_pointUp;
                ++p_pointDown;

                const Vector3f& point = *p_point;
//                const Vector3f& pointLeft = *p_pointLeft;
                const Vector3f& pointRight = *p_pointRight;
//                const Vector3f& pointUp = *p_pointUp;
                const Vector3f& pointDown = *p_pointDown;

                if (point.z != 0 &&
                    pointRight.z != 0 &&
                    pointDown.z != 0// &&
                    //pointLeft.z != 0 &&
                    //pointUp.z != 0
                    )
                {
                    Vector3f v1 = pointRight - point;
                    Vector3f v2 = pointDown - point;

                    *normMap = v2.cross(v1);
                }
                else
                {
                    *normMap = Vector3f::zero();
                }

                ++normMap;
            }

            //last pixel at end of row
            *normMap = Vector3f::zero();
            ++normMap;
        }

        //bottom row
        for (int x = 0; x < width; ++x)
        {
            *normMap = Vector3f::zero();
            ++normMap;
        }

        //box_blur(normalMap_.get(), blurNormalMap_.get(), width, height, blurRadius_);
        box_blur_fast(normalMap_.get(), blurNormalMap_.get(), width, height);
    }

    void LitDepthVisualizer::prepare_buffer(size_t width, size_t height, SensorData& data)
    {
        PROFILE_FUNC();
        if (data.depthRGBData == nullptr || width != data.depthWidth || height != data.depthHeight)
        {
            data.depthWidth = width;
            data.depthHeight = height;
            data.depthRGBData = new int8_t[width * height * sizeof(astra_rgb_pixel_t)];
        }
    }

    inline uint8_t ClampByte(int n)
    {
        n &= -(n >= 0);
        return n | ((255 - n) >> 31);
    }

    inline float Clamp01(float value)
    {
        if (value > 1.0f)
            return 1.0f;
        else if (value < 0.0f)
            return 0.0f;
        else
            return value;
    }

    void LitDepthVisualizer::updateLitPoints(const astra::PointFrame& pointFrame, SensorData& data)
    {
        PROFILE_FUNC();
        calculate_normals(pointFrame);

        PROFILE_BEGIN(setup);

        const size_t width = pointFrame.width();
        const size_t height = pointFrame.height();

        prepare_buffer(width, height, data);

        const Vector3f* pointData = pointFrame.data();

        astra_rgb_pixel_t* texturePtr = reinterpret_cast<astra_rgb_pixel_t*>(data.depthRGBData);

        const Vector3f* normMap = blurNormalMap_.get();

        const float lightGray = lightColor_.r;
        const float ambientGray = ambientColor_.r;
        const float farDepthInv = 1.0f / 4000.0f;
        const int numPoints = width * height;
        PROFILE_END();

        PROFILE_BEGIN(loop);
        for (int i = 0; i < numPoints; ++i, ++pointData, ++normMap, ++texturePtr)
        {
            float depth = (*pointData).z;

            if (depth != 0)
            {
                const float fadeFactor = 1.0f - 0.6f*Clamp01(depth * farDepthInv);

                const Vector3f norm = Vector3f::normalize(*normMap);
                const float diffuseFactor = norm.dot(lightVector_);

                uint8_t diffuseGray = 0;

                if (diffuseFactor > 0.0f)
                {
                    //only add diffuse when mesh is facing the light
                    diffuseGray = lightGray * diffuseFactor;
                }

                const uint8_t value = ClampByte(fadeFactor * (ambientGray + diffuseGray));
                texturePtr->r = value;
                texturePtr->g = value;
                texturePtr->b = value;
            }
            else
            {
                texturePtr->r = 0;
                texturePtr->g = 0;
                texturePtr->b = 0;
            }
        }
        PROFILE_END();
    }

    void LitDepthVisualizer::update_debug_handframe(const astra::DebugHandFrame& debugHandFrame, SensorData& data)
    {
        PROFILE_FUNC();

        const size_t width = debugHandFrame.width();
        const size_t height = debugHandFrame.height();

        prepare_buffer(width, height, data);

        astra::RgbPixel* texturePtr = reinterpret_cast<astra::RgbPixel*>(data.depthRGBData);

        const astra::RgbPixel* imagePtr = debugHandFrame.data();

        const int length = width * height * sizeof(RgbPixel);
        memcpy(texturePtr, imagePtr, length);
    }

    void LitDepthVisualizer::updateDepth(const astra::DepthFrame& depthFrame, SensorData& data)
    {
        PROFILE_FUNC();

        const size_t width = depthFrame.width();
        const size_t height = depthFrame.height();

        prepare_buffer(width, height, data);
        const int numPoints = width * height;

        const int16_t* depthData = depthFrame.data();
        astra_rgb_pixel_t* texturePtr = reinterpret_cast<astra_rgb_pixel_t*>(data.depthRGBData);

        for (int i = 0; i < numPoints; ++i, ++depthData, ++texturePtr)
        {
            float depth = static_cast<float>(*depthData);

            if (depth != 0)
            {
                const float normDepth = (depth - 400.0f) / 3200.0f;
                const float normValue = 1.0f - 0.8f*Clamp01(normDepth);
                const uint8_t value = ClampByte(255 * normValue);

                texturePtr->r = value;
                texturePtr->g = value;
                texturePtr->b = value;
            }
            else
            {
                texturePtr->r = 0;
                texturePtr->g = 0;
                texturePtr->b = 0;
            }
        }
    }
}}

#endif /* LITDEPTHVISUALIZER_HPP */
