#include "Renderer.h"
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>

bool checkGlError(const char* funcName)
{
    GLint err = glGetError();
    if (err != GL_NO_ERROR) {
        LOGE("GL error after %s(): 0x%08x\n", funcName, err);
        return true;
    }
    return false;
}

GLuint createShader(GLenum shaderType, const char* src)
{
    GLuint shader = glCreateShader(shaderType);
    if (!shader) {
        checkGlError("glCreateShader");
        return 0;
    }
    glShaderSource(shader, 1, &src, NULL);

    GLint compiled = GL_FALSE;
    glCompileShader(shader);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (!compiled) {
        GLint infoLogLen = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLen);
        if (infoLogLen > 0) {
            GLchar* infoLog = (GLchar*)malloc(infoLogLen);
            if (infoLog) {
                glGetShaderInfoLog(shader, infoLogLen, NULL, infoLog);
                LOGE("Could not compile %s shader:\n%s\n",
                        shaderType == GL_VERTEX_SHADER ? "vertex" : "fragment",
                        infoLog);
                free(infoLog);
            }
        }
        glDeleteShader(shader);
        return 0;
    }

    return shader;
}

GLuint createProgram(const char* vtxSrc, const char* fragSrc)
{
    GLuint vtxShader = 0;
    GLuint fragShader = 0;
    GLuint program = 0;
    GLint linked = GL_FALSE;

    vtxShader = createShader(GL_VERTEX_SHADER, vtxSrc);
    if (!vtxShader)
        goto exit;

    fragShader = createShader(GL_FRAGMENT_SHADER, fragSrc);
    if (!fragShader)
        goto exit;

    program = glCreateProgram();
    if (!program) {
        checkGlError("glCreateProgram");
        goto exit;
    }
    glAttachShader(program, vtxShader);
    glAttachShader(program, fragShader);

    glLinkProgram(program);
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (!linked) {
        LOGE("Could not link program");
        GLint infoLogLen = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLen);
        if (infoLogLen) {
            GLchar* infoLog = (GLchar*)malloc(infoLogLen);
            if (infoLog) {
                glGetProgramInfoLog(program, infoLogLen, NULL, infoLog);
                LOGE("Could not link program:\n%s\n", infoLog);
                free(infoLog);
            }
        }
        glDeleteProgram(program);
        program = 0;
    }

exit:
    glDeleteShader(vtxShader);
    glDeleteShader(fragShader);
    return program;
}

Renderer::Renderer()
:   mLastFrameNs(0),
    mEglContext(eglGetCurrentContext()),
    programObject(0)
{
}

Renderer::~Renderer()
{
    /* The destructor may be called after the context has already been
     * destroyed, in which case our objects have already been destroyed.
     *
     * If the context exists, it must be current. This only happens when we're
     * cleaning up after a failed init().
     */
    if (eglGetCurrentContext() != mEglContext)
        return;

    delete_texture();

    // Delete program object
    glDeleteProgram(programObject);
    glDeleteProgram(lineProgramObject);
}

void Renderer::resize(int width, int height)
{
    mLastFrameNs = 0;

    glViewport(0, 0, width, height);
}

bool Renderer::init()
{
    const char vShaderStr[] =
          "attribute vec4 a_position;   \n"
          "attribute vec2 a_texCoord;   \n"
          "varying vec2 v_texCoord;     \n"
          "void main()                  \n"
          "{                            \n"
          "   gl_Position = a_position; \n"
          "   v_texCoord = a_texCoord;  \n"
          "}                            \n";

    const char fShaderStr[] =
      "precision mediump float;                            \n"
      "varying vec2 v_texCoord;                            \n"
      "uniform sampler2D s_texture;                        \n"
      "void main()                                         \n"
      "{                                                   \n"
      "  gl_FragColor = texture2D( s_texture, v_texCoord );\n"
      "}                                                   \n";

    const char lineVertexShaderStr[] =
        "attribute vec4 a_position;   \n"
        "attribute vec4 a_color;      \n"
        "varying vec4 v_color;        \n"
        "void main()                  \n"
        "{                            \n"
        "   gl_Position = a_position; \n"
        "   v_color = a_color;        \n"
        "}                            \n";

    const char lineFragmentShaderStr[] =
    "precision mediump float;                            \n"
    "varying vec4 v_color;                               \n"
    "void main()                                         \n"
    "{                                                   \n"
    "  gl_FragColor = v_color;                           \n"
    "}                                                   \n";

    // Load the shaders and get a linked program object
    programObject = createProgram ( vShaderStr, fShaderStr );

    // Get the attribute locations
    positionLoc = glGetAttribLocation ( programObject, "a_position" );
    texCoordLoc = glGetAttribLocation ( programObject, "a_texCoord" );

    // Get the sampler location
    samplerLoc = glGetUniformLocation ( programObject, "s_texture" );


    lineProgramObject = createProgram (lineVertexShaderStr, lineFragmentShaderStr);
    linePositionLoc = glGetAttribLocation( lineProgramObject, "a_position");
    lineColorLoc = glGetAttribLocation( lineProgramObject, "a_color");

    glClearColor ( 0.0f, 0.0f, 0.0f, 0.0f );
    return true;
}

void Renderer::draw_rectangle(astra::Vector2f& point1,
                              astra::Vector2f& point2,
                              float thickness)
{
    float r = 0.0f;
    float g = 0.0f;
    float b = 1.0f;
    float a = 1.0f;

    astra::Vector2f direction = point2 - point1;
    auto unitDirection = direction / std::sqrt(direction.x*direction.x + direction.y*direction.y);
    astra::Vector2f unitPerpendicular(-unitDirection.y, unitDirection.x);

    auto offset = (thickness / 2.f)*unitPerpendicular;

    auto p1 = point1 + offset;
    auto p2 = point2 + offset;
    auto p3 = point2 - offset;
    auto p4 = point1 - offset;

    GLfloat vVertices[] = { p1.x,  p1.y, 0.0f,  // Position 0
                            r, g, b, a,        // Color 0
                            p2.x,  p2.y, 0.0f,  // Position 1
                            r, g, b, a,        // Color 1
                            p3.x,  p3.y, 0.0f,  // Position 2
                            r, g, b, a,        // Color 2
                            p4.x,  p4.y, 0.0f,  // Position 3
                            r, g, b, a,        // Color 3
                          };
    GLushort indices[] = { 0, 1, 2, 0, 2, 3 };

    glUseProgram ( lineProgramObject );

    // Load the vertex position
    glVertexAttribPointer ( linePositionLoc, 3, GL_FLOAT,
                           GL_FALSE, 7 * sizeof(GLfloat), vVertices );
    // Load the texture coordinate
    glVertexAttribPointer ( lineColorLoc, 4, GL_FLOAT,
                           GL_FALSE, 7 * sizeof(GLfloat), &vVertices[3] );

    glEnableVertexAttribArray ( linePositionLoc );
    glEnableVertexAttribArray ( lineColorLoc );

    glDrawElements ( GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, indices );
}

void Renderer::draw_hand_trace(SensorData& data, const PointList& pointList)
{
    if (pointList.size() < 2)
    {
        return;
    }

    auto it = pointList.begin();

    astra::Vector2i lastPoint = *it;
    ++it;

    const float depthScaleWidth = 2.0f / data.depthWidth;
    const float depthScaleHeight = 2.0f / data.depthHeight;
    const float thickness = 0.0025;

    while (it != pointList.end())
    {
        astra::Vector2i currentPoint = *it;
        ++it;

        astra::Vector2f p1((lastPoint.x + 0.5) * depthScaleWidth - 1.0f,
                              1.0f - (lastPoint.y + 0.5) * depthScaleHeight);
        astra::Vector2f p2((currentPoint.x + 0.5) * depthScaleWidth - 1.0f,
                              1.0f - (currentPoint.y + 0.5) * depthScaleHeight);
        lastPoint = currentPoint;

        draw_rectangle(p1, p2, thickness);
    }
}

void Renderer::render(SensorData& data)
{
    update_depth(data);

    GLfloat vVertices[] = { -1.0f,  1.0f, 0.0f,  // Position 0
                            0.0f,  0.0f,        // TexCoord 0
                           -1.0f, -1.0f, 0.0f,  // Position 1
                            0.0f,  1.0f,        // TexCoord 1
                            1.0f, -1.0f, 0.0f,  // Position 2
                            1.0f,  1.0f,        // TexCoord 2
                            1.0f,  1.0f, 0.0f,  // Position 3
                            1.0f,  0.0f         // TexCoord 3
                         };
    GLushort indices[] = { 0, 1, 2, 0, 2, 3 };

    // Clear the color buffer
    glClear ( GL_COLOR_BUFFER_BIT );

    // Use the program object
    glUseProgram ( programObject );

    // Load the vertex position
    glVertexAttribPointer ( positionLoc, 3, GL_FLOAT,
                           GL_FALSE, 5 * sizeof(GLfloat), vVertices );
    // Load the texture coordinate
    glVertexAttribPointer ( texCoordLoc, 2, GL_FLOAT,
                           GL_FALSE, 5 * sizeof(GLfloat), &vVertices[3] );

    glEnableVertexAttribArray ( positionLoc );
    glEnableVertexAttribArray ( texCoordLoc );

    // Bind the texture
    if (pixels_ != nullptr)
    {
        glActiveTexture ( GL_TEXTURE0 );
        glBindTexture ( GL_TEXTURE_2D, textureId_ );
    }

    // Set the sampler texture unit to 0
    glUniform1i ( samplerLoc, 0 );

    glDrawElements ( GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, indices );

    astra::Vector2f p1(-0.3f,-0.1f);
    astra::Vector2f p2(0.3f,0.1f);

    for (auto it : data.pointMap)
    {
        PointList& list = it.second;
        draw_hand_trace(data, list);
    }
}

void Renderer::update_depth(SensorData& data)
{
    if (!data.isValid)
    {
        return;
    }

    if (data.depthWidth != texWidth_ || data.depthHeight != texHeight_ || pixels_ == nullptr)
    {
        LOGI("creating new texture with size %d by %d", data.depthWidth, data.depthHeight);
        textureId_ = create_texture(data);
    }
    else
    {
        update_texture(data);
        upload_texture();
    }
}

void Renderer::update_texture(SensorData& data)
{
    const int bytesPerPixel = 3;
    const int byteLength = texWidth_ * texHeight_ * bytesPerPixel;

    if (showDepthVisualization_)
    {
        memcpy(pixels_, data.depthRGBData, byteLength);
    }
    else
    {
        memset(pixels_, 0, byteLength);
    }

    render_handpoints(data);
}

void Renderer::render_handpoints(SensorData& data)
{
    const int width = data.depthWidth;
    const int height = data.depthHeight;

    for (auto handPoint : data.handPoints)
    {
        const astra::Vector2i& p = handPoint.depth_position();

        const int bytesPerPixel = 3;

        auto status = handPoint.status();
        bool isTrackingPoint = status == HAND_STATUS_TRACKING;
        bool isLostTrackingPoint = status == HAND_STATUS_LOST;

        int y0 = std::max(0, p.y - 1);
        int y1 = std::min(height - 1, p.y + 1);
        int x0 = std::max(0, p.x - 1);
        int x1 = std::min(width - 1, p.x + 1);

        for (int y = y0; y <= y1; y++)
        {
            for (int x = x0; x <= x1; x++)
            {
                uint8_t r = 0;
                uint8_t g = 0;
                uint8_t b = 0;

                if ((y == p.y || x == p.x))
                {
                    if (isLostTrackingPoint)
                    {
                        r = 255;
                        g = 0;
                        b = 0;
                    }
                    else if (isTrackingPoint)
                    {
                        r = 0;
                        g = 204;
                        b = 0;
                    }
                    else
                    {
                        r = 255;
                        g = 255;
                        b = 0;
                    }
                }
                else
                {
                    r = 0;
                    g = 0;
                    b = 0;
                }

                int index = x + y * width;
                uint8_t* pixel = pixels_ + index * bytesPerPixel;

                *(pixel) = r;
                *(pixel + 1) = g;
                *(pixel + 2) = b;
            }
        }
    }
}

void Renderer::upload_texture()
{
    glBindTexture ( GL_TEXTURE_2D, textureId_ );

    glTexSubImage2D ( GL_TEXTURE_2D, 0, 0, 0,
                    texWidth_,
                    texHeight_,
                    GL_RGB,
                    GL_UNSIGNED_BYTE,
                    pixels_);
}

void Renderer::delete_texture()
{
    if (pixels_ != nullptr)
    {
        delete[] pixels_;
        pixels_ = nullptr;
        glDeleteTextures ( 1, &textureId_ );
    }
}

GLuint Renderer::create_texture(SensorData& data)
{
    delete_texture();

    texWidth_ = data.depthWidth;
    texHeight_ = data.depthHeight;

    int bytesPerPixel = 3;
    int byteLength = texWidth_ * texHeight_ * bytesPerPixel;

    pixels_ = new uint8_t[byteLength];

    update_texture(data);

    // Texture object handle
    GLuint textureId;

    // Use tightly packed data
    glPixelStorei ( GL_UNPACK_ALIGNMENT, 1 );

    // Generate a texture object
    glGenTextures ( 1, &textureId );

    // Bind the texture object
    glBindTexture ( GL_TEXTURE_2D, textureId );

    // Load the texture
    glTexImage2D ( GL_TEXTURE_2D, 0, GL_RGB, texWidth_, texHeight_, 0, GL_RGB, GL_UNSIGNED_BYTE, pixels_);

    // Set the filtering mode
    glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
    glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

    return textureId;
}
