#ifndef SENSORDATA_H
#define SENSORDATA_H

#include <astra/astra.hpp>
#include <deque>
#include <unordered_map>

using PointList = std::deque <astra::Vector2i>;
using PointMap = std::unordered_map <int, PointList>;

class SensorData {
public:
    std::vector<astra::HandPoint> handPoints;

    int depthWidth{0};
    int depthHeight{0};
    int8_t* depthRGBData{nullptr};
    bool isValid {false};

    PointMap pointMap;
};

#endif //SENSORDATA_H
