#ifndef ASTRATEST_H
#define ASTRATEST_H

#include <astra/astra.hpp>
#include "Renderer.h"
#include "common.h"
#include "SensorData.h"
#include "lit_depth_visualizer.hpp"
#include <deque>
#include <unordered_map>
#include <chrono>
#include <Shiny.h>

static bool g_astra_initialized = false;

class AstraTest
{
public:
    AstraTest()
    {
        PROFILE_FUNC();
        init_astra_once();

        start();
    }

    ~AstraTest()
    {
        PROFILE_FUNC();
        stop();
    }

    void update(SensorData& data)
    {
        PROFILE_FUNC();

        PROFILE_BEGIN(astra_update);
        astra_update();
        PROFILE_END();

        PROFILE_BEGIN(get_latest_frame);
        astra::Frame frame = reader_.get_latest_frame(0);
        PROFILE_END();

        if (frame.is_valid())
        {
            data.isValid = true;
            //these methods could mark isValid as false if they fail
            processdepthframe(frame, data);
            //processpointframe(frame, data);
            processHandFrame(frame, data);
            //processdebug_handframe(frame, data);

            if (data.isValid)
            {
                check_fps();
            }
        }
        else
        {
            data.isValid = false;
        }
    }

private:
    void init_astra_once()
    {
        PROFILE_FUNC();
        if (g_astra_initialized)
        {
            return;
        }
        g_astra_initialized = true;
        LOGI("astra_initialize");
        astra::initialize();
    }

    void start()
    {
        PROFILE_FUNC();
        sensor_ = SensorPtr(new astra::StreamSet());
        reader_ = sensor_->create_reader();

        reader_.stream<astra::DepthStream>().start();
        //reader_.stream<astra::pointstream>().start();

        auto handStream = reader_.stream<astra::HandStream>();
        handStream.start();
        handStream.set_include_candidate_points(true);

        //auto debugHandStream = reader_.stream<astra::DebugHandStream>();
        //debugHandStream.start();
        //debugHandStream.set_view_type(DEBUG_HAND_VIEW_DEPTH);

        lastTimepoint_ = clock_type::now();
        lastOutput_ = clock_type::now();
    }

    void stop()
    {
        PROFILE_FUNC();
        reader_.stream<astra::DepthStream>().stop();
        //reader_.stream<astra::PointStream>().stop();
        reader_.stream<astra::HandStream>().stop();
        //reader_.stream<astra::DebugHandStream>().stop();
    }

    void processdepthframe(astra::Frame& frame, SensorData& data)
    {
        PROFILE_FUNC();
        const astra::DepthFrame depthFrame = frame.get<astra::DepthFrame>();

        if (!depthFrame.is_valid())
        {
            data.isValid = false;
            return;
        }

        visualizer_.updateDepth(depthFrame, data);
    }

    void processpointframe(astra::Frame& frame, SensorData& data)
    {
        PROFILE_FUNC();
        const astra::PointFrame pointFrame = frame.get<astra::PointFrame>();

        if (!pointFrame.is_valid())
        {
            data.isValid = false;
            return;
        }

        visualizer_.updateLitPoints(pointFrame, data);
    }

    PointList update_hand_trace(SensorData& data, int trackingId, const astra::Vector2i& position)
    {
        auto it = pointMap_.find(trackingId);
        if (it == pointMap_.end())
        {
            PointList list;
            for (int i = 0; i < maxTraceLength_; ++i)
            {
                list.push_back(position);
            }
            pointMap_.insert(std::make_pair(trackingId, list));
            return list;
        }
        else
        {
            PointList& list = it->second;
            while (list.size() < maxTraceLength_)
            {
                list.push_back(position);
            }
            return list;
        }
    }

    void shorten_hand_traces(SensorData& data)
    {
        auto it = pointMap_.begin();

        while (it != pointMap_.end())
        {
            PointList& list = it->second;
            if (list.size() > 1)
            {
                list.pop_front();
                ++it;
            }
            else
            {
                it = pointMap_.erase(it);
            }
        }
    }

    void processHandFrame(astra::Frame& frame, SensorData& data)
    {
        PROFILE_FUNC();
        const astra::HandFrame handFrame = frame.get<astra::HandFrame>();
        if (!handFrame.is_valid())
        {
            data.isValid = false;
            return;
        }
        auto handPoints = handFrame.handpoints();

        data.handPoints = handPoints;


        shorten_hand_traces(data);

        data.pointMap.clear();

        for (auto handPoint : handPoints)
        {
            if (handPoint.status() == HAND_STATUS_TRACKING)
            {
                auto trackingId = handPoint.tracking_id();
                PointList list = update_hand_trace(data, trackingId, handPoint.depth_position());

                //insert a clone of the latest list
                //so the renderer can use it's own pointMap on another thread
                data.pointMap.insert(std::make_pair(trackingId, list));
            }
        }
    }

    void processdebug_handframe(astra::Frame& frame, SensorData& data)
    {
        const astra::DebugHandFrame debugHandFrame = frame.get<astra::DebugHandFrame>();

        if (!debugHandFrame.is_valid())
        {
            data.isValid = false;
            return;
        }

        visualizer_.update_debug_handframe(debugHandFrame, data);
    }

    void check_fps()
    {
        PROFILE_FUNC();
        const double frameWeight = 0.2;

        auto newTimepoint = clock_type::now();
        auto frameDuration = std::chrono::duration_cast<duration_type>(newTimepoint - lastTimepoint_);
        auto outputDuration = std::chrono::duration_cast<duration_type>(newTimepoint - lastOutput_);

        frameDuration_ = frameDuration * frameWeight + frameDuration_ * (1 - frameWeight);
        lastTimepoint_ = newTimepoint;

        if (outputDuration.count() > 1)
        {
            lastOutput_ = newTimepoint;
            double fps = 1.0 / frameDuration_.count();

            LOGI("Sensor FPS: %.3f Duration: %.2f ms", fps, frameDuration_.count() * 1000);
        }
    }

    samples::common::LitDepthVisualizer visualizer_;

    using SensorPtr = std::unique_ptr<astra::StreamSet> ;
    SensorPtr sensor_{nullptr};
    astra::StreamReader reader_;

    using clock_type = std::chrono::system_clock;
    std::chrono::time_point<clock_type> lastTimepoint_;
    std::chrono::time_point<clock_type> lastOutput_;

    using duration_type = std::chrono::duration<double>;
    duration_type frameDuration_{0.0};

    int maxTraceLength_{ 15 };

    PointMap pointMap_;
};

#endif
