LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

# Sources
MY_SRC_FILES := \
  $(LOCAL_PATH)/*.cpp

MY_SRC_FILE_EXPANDED := $(wildcard $(MY_SRC_FILES))
LOCAL_SRC_FILES := $(MY_SRC_FILE_EXPANDED:$(LOCAL_PATH)/%=%)

# Includes

# Output
LOCAL_SHARED_LIBRARIES := libastra_core_api libastra_core libastra libShiny
LOCAL_LDLIBS := -llog -landroid -lEGL -lGLESv2
LOCAL_MODULE := libSensorViewer

include $(BUILD_SHARED_LIBRARY)
