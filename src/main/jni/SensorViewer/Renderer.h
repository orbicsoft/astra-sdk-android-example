#ifndef RENDERER_H
#define RENDERER_H

#include <stdint.h>
#include <EGL/egl.h>
#include <GLES2/gl2.h>

#include "common.h"
#include <math.h>
#include "SensorData.h"

#define TWO_PI 6.28318530718

class Renderer {
public:
    Renderer();
    virtual ~Renderer();
    void resize(int width, int height);
    void render(SensorData& data);
    bool init();
    void toggle_depth_visualization() { showDepthVisualization_ = !showDepthVisualization_; }

private:
    GLuint create_texture(SensorData& data);
    void delete_texture();
    void update_depth(SensorData& data);
    void update_texture(SensorData& data);
    void upload_texture();
    void render_handpoints(SensorData& data);
    void check_fps();
    void draw_rectangle(astra::Vector2f& point1,
                        astra::Vector2f& point2,
                        float thickness);
    void draw_hand_trace(SensorData& data, const PointList& pointList);

    uint64_t mLastFrameNs;

    //gl2
    const EGLContext mEglContext;

    // Handle to a program object
    GLuint programObject;
    GLuint lineProgramObject;

    // Attribute locations
    GLint  linePositionLoc;
    GLint  lineColorLoc;
    GLint  positionLoc;
    GLint  texCoordLoc;

    // Sampler location
    GLint samplerLoc;

    // Texture handle
    GLuint textureId_;

    uint8_t* pixels_{nullptr};
    int32_t texWidth_{128};
    int32_t texHeight_{128};
    bool showDepthVisualization_{true};
};

#endif
