# core
include $(CLEAR_VARS)

LOCAL_MODULE := astra_core

LOCAL_SRC_FILES := $(ASTRA_ANDROID_SDK)/lib/libastra_core.so
LOCAL_EXPORT_C_INCLUDES := $(ASTRA_ANDROID_SDK)/include

include $(PREBUILT_SHARED_LIBRARY)

# api

include $(CLEAR_VARS)

LOCAL_MODULE := astra_core_api

LOCAL_SRC_FILES := $(ASTRA_ANDROID_SDK)/lib/libastra_core_api.so
LOCAL_EXPORT_C_INCLUDES := $(ASTRA_ANDROID_SDK)/include

include $(PREBUILT_SHARED_LIBRARY)

# astra

include $(CLEAR_VARS)

LOCAL_MODULE := astra

LOCAL_SRC_FILES := $(ASTRA_ANDROID_SDK)/lib/libastra.so
LOCAL_EXPORT_C_INCLUDES := $(ASTRA_ANDROID_SDK)/include

include $(PREBUILT_SHARED_LIBRARY)
